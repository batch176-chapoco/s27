/* let http = require("http");

http.createServer(function (req, res) {

    console.log(req.url);
    console.log(req.method);

    if (req.url === "/" && req.method === "GET") {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end("This is a response to a GET Method request");
    } else if (req.url === "/" && req.method === "POST") {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end("This is a response to a POST method request");
    } else if (req.url === "/" && req.method === "PUT") {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end("This is a response to a PUT method request");
    } else if (req.url === "/" && req.method === "DELETE") {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end("This is a response to a DELETE method request");
    } else if (req.url === "/courses" && req.method === "GET") {
        res.writeHead(200, { 'Content-Type': 'type/plain' });
        res.end("This is a GET method request for the /courses endpoint.");
    } else if (req.url === "/courses" && req.method === "POST") {
        res.writeHead(200, { 'Content-Type': 'type/plain' });
        res.end("This is a POST method request for the /courses endpoint.")
    }


}).listen(4000);

console.log("Server is running at localhost:4000");


 */

let http = require("http");

//Mock Data
let products = [

    {
        name: "Iphone X",
        description: "Phone designed and created by Apple",
        price: 30000
    },
    {
        name: "Horizon Forbidden West",
        description: "Newest game for the PS4 and PS5",
        price: 4000
    },
    {
        name: "Razer Tiamat",
        description: "Headset from Razer",
        price: 3000
    }

];