let http = require("http");

let courses = [

    {
        name: "English 101",
        price: "23000",
        isActive: true
    },
    {
        name: "Math 101",
        price: 25000,
        isActive: true
    },
    {
        name: "Reading 101",
        price: 21000,
        isActive: true
    }

];

http.createServer(function (req, res) {

    //HTTP Methods allow us to identify what action to take for our
    //request.
    //With an HTTP Method we can actually routes that caters to the same
    //endpoint but with a different action.
    //Most HTTP Methods are primarily concerned with CRUD operations:
    //Common HTTP Methods:
    //GET = GET method request indicates that the client wants to retrieve
    //or get data.
    //POST = POST method request indicates that the clients wants to send
    //data to create a resource.
    //PUT = PUT method request indicates that the client wants to send data
    //to update a resource.
    //DELETE = DELETE method request indicates that the clients wants to delete
    //a resource.

    console.log(req.url); //the request URL endpoint
    console.log(req.method); //the method of the request

    if (req.url === "/" && req.method === "GET") {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end("This is a response to a GET Method request");
    } else if (req.url === "/" && req.method === "POST") {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end("This is a response to a POST method request");
    } else if (req.url === "/" && req.method === "PUT") {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end("This is a response to a PUT method request");
    } else if (req.url === "/" && req.method === "DELETE") {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end("This is a response to a DELETE method request");
    } else if (req.url === "/courses" && req.method === "GET") {

        //Since we are now passing a stringified JSON, so that the client
        //which will receive our response be able to display our data
        //properly, we have to update our Content-Type headers to
        //application/json
        res.writeHead(200, { 'Content-Type': 'application/json' });
        //We cannot pass another data type other than a string for our end().
        //So, to be able to pass our array, we have to transform into a JSON string.

        res.end(JSON.stringify(courses));
    } else if (req.url === "/courses" && req.method === "POST") {

        //This route should be able to receive data from the client and
        // we should be able to create a new course and add it to our
        //courses array.

        //This will act as a placeholder to contain the data passed from
        //the client.
        let requestBody = "";

        //Receiving data from a client to a nodeJS server requires 2 steps:

        //data step - this part will read the stream of data coming from our
        //client and process the incoming data into the requestBody 
        //variable.

        req.on('data', function (data) {

            //console.log(data);//stream of data from client
            requestBody += data //data stream is saved into the variable
        })

        //end step - this will run once or after the request data has
        //been completely sent from our client.

        req.on('end', function () {
            // console.log(requestBody);
            //requestBody now contains data from our Postman client
            //since requestBody is JSON format we have to parse to add it
            //as an object in our array.
            requestBody = JSON.parse(requestBody);

            //Simulate creating document and adding it in a collection
            let newCourse = {

                name: requestBody.name,
                price: requestBody.price,
                isActive: true
            }

            console.log(newCourse);
            courses.push(newCourse);//check if the new course was added
            //into the courses array
            console.log(courses);

        })

        res.writeHead(200, { 'Content-Type': 'application/json' });
        //send the updated courses array to the client as a response
        res.end(JSON.stringify(courses));
    }

    /* 
        Mini Activity

        Create a new route for the following endpoint: "/courses"

        1. This route should be "GET" method route
            -Add our status code, 200 and our Content Type. textplain.
            -Add an end() method to end the response with the following
            message:
            "This is a GET method request for the /courses endpoint."
        
        2. This route should be "POST" method route
            -Add our status code, 200 and our Content Type. textplain.
            -Add an end() method to end the response with the following
            message:
            "This is a POST method request for the /courses endpoint."
    */





}).listen(4000);

console.log("Server is running at localhost:4000");